## FreeBSD Grapejuice Documentation
A few documents on getting Grapejuice running on FreeBSD
# 1. Installing wine
You'll need to install the 32-bit wine package. Install it with the following command:

    pkg install i386-wine-devel
# 2. Installing Python dependencies
Now, you'll need to install the Python dependencies required for Grapejuice. Install the following packages:

    pkg install git py38-pip cairo gtk3 gobject-introspection desktop-file-utils xdg-utils xdg-user-dirs gtk-update-icon-cache shared-mime-info python-38
# 3. Install Grapejuice
Now that you've got the required packages and dependencies ready, you'll need to install Grapejuice
## 1. Clone the git repository
Now, move onto cloning the git repository.

    git clone https://gitlab.com/brinkervii/grapejuice.git /tmp/grapejuice
## 2. Run the installer file
We'll now need to run the install.py file. First, change your directory to /tmp/grapejuice:

    cd /tmp/grapejuice
Edit the install.py file and go to line 14  
<br />Then, replace `expected_architecture = "x86_64"` with `expected_architecture = "amd64"`

Next, you'll run the install.py file:

    python3 ./install.py
# 4. Configuring Grapejuice
Now that you've got Grapejuice installed, we'll need to do a bit of configuring to get it to work.
## 1. Editing the configuration file for Grapejuice.
We'll need to make a few modifications here to make Grapejuice work with our installed wine
Since Grapejuice will look for wine in /usr/bin, we'll need to edit the wine_binary field. Wine in FreeBSD does not support WoW64, so you'll need to add an env variable  
<br />Make sure your configuration file in `~/.config/brinkervii/grapejuice/user_settings.json` looks like the following:

    {
      "show_fast_flag_warning": true,
      "wine_binary": "/usr/local/bin/wine",
      "dll_overrides": "ucrtbase=n,b;api-ms-win-crt-private-l1-1-0=n,b;dxdiagn=;winemenubuilder.exe=",
      "no_daemon_mode": true,
      "release_channel": "master",
      "env": {
        "WINEARCH": "win32"
      },
      "enabled_tweaks": [""],
      "disable_updates": false
    }
## 2. Installing Roblox
Now, you are gonna install Roblox, open Grapejuice with `~/.local/bin/grapejuice gui`, then go to Maintenance and hit "Install Roblox" Once it's done, move onto the next step
## 3. Linking Program Files to Program Files (x86) on the Grapejuice wineprefix
You'll need to symlink Program Files to Program Files (x86) on the wineprefix, because Grapejuice looks for Roblox in the Program Files (x86) folder. Since it's not there by default due to 32-bit wine, you'll need to link them

    ln -s "~/.local/share/grapejuice/wineprefix/drive_c/Program Files" "~/.local/share/grapejuice/wineprefix/drive_c/Program Files (x86)"
# 5. Play Roblox
Now, you could play Roblox through your favorite browser. For more documentation, read the files in the more_info/ directory of this repository.
