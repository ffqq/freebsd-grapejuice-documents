### Changing the renderer

Now that you've got Roblox installed, you'll experience horrible FPS due to the D3D11 translation method that wined3d uses.

### ***! Before reading, do not blindly copy and paste stuff shown here !***

## 1. Install studio

We will need to install Studio, click the "Roblox Studio" button on Grapejuice's GUI![](https://cdn.discordapp.com/attachments/858405184358842408/879328353243525150/unknown.png)
## 2. Open the FFlag Editor
Now we'll be opening the FFlag editor. Click on the FFlag editor button in the GUI. Click "Open Editor" on the warning![](https://cdn.discordapp.com/attachments/858405184358842408/879329482312384572/unknown.png)
## 3. Enabling your renderer
Now, we'll be enabling the rendered. As of now, you have 3 choices. OpenGL, Vulkan, and D3D11. Search up your preferred editor "FFlagDebugGraphicsPreferFoo" and enable it, then save **(REPLACE FOO WITH YOUR RENDERER)**![](https://cdn.discordapp.com/attachments/858405184358842408/879330542036848671/unknown.png)
## End of Document

Experiment with other renderers to see which one milks the most juice out.
